# hercules

## Introduction

The sections in this repository contain templates and code to provision, configure and package infrastructure artifacts on a variety of infrastructure platforms. The goal in each section is to use the major components in each technology to demonstrate both its specific function as well as its integration with the deployment as a whole. 
The entities will be simple but non-trivial, each satisfying a specific requirement while remaining loosely coupled so that they can be used independently in other projects.   

We will use [**Packer**](https://www.packer.io/), [**Terraform**](https://www.terraform.io/) and [**Ansible**](https://www.ansible.com/) for packaging, provisioning and configuring various resources, platforms and applications. 

## Prerequisites 

You will need Packer, Ansible and Terraform installed on your machine. On macOS, all these packages are available from Homebrew.

```zsh
brew install packer ansible terraform
```

In each of the project folders, you'll need to run `terraform init` to fetch the modules Terraform uses in that section.
