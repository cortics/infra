resource "aws_efs_file_system" "TestEfs" {
  creation_token   = "TestEfs"
  encrypted        = "true"
  kms_key_id       = "${aws_kms_key.TestKMSKey.arn}"
  performance_mode = "generalPurpose"

  tags {
    Name = "TestEfs"
  }
}

resource "aws_efs_mount_target" "TestEfsMountTarget" {
  file_system_id  = "${aws_efs_file_system.TestEfs.id}"
  subnet_id       = "${aws_subnet.TestPrivateSubnet.id}"
  ip_address      = "10.62.17.20"
  security_groups = ["${aws_security_group.TestSecurityGroup.id}"]
}
