resource "aws_eip" "TestNatEip" {
  vpc        = true
  depends_on = ["aws_internet_gateway.TestIgw"]

  tags {
    Name = "TestNatEip"
  }
}

resource "aws_eip" "TestPublicInstanceEip" {
  vpc               = true
  network_interface = "${aws_network_interface.TestPublicNetworkInterface.id}"
  depends_on        = ["aws_internet_gateway.TestIgw"]

  tags {
    Name = "TestPublicInstanceEip"
  }
}
