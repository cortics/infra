resource "aws_route53_zone" "TestHostedZone" {
  name          = "testhostedzone.internal"
  vpc_id        = "${aws_vpc.TestVPC.id}"
  force_destroy = "true"

  tags {
    Name = "TestHostedZone"
  }
}

resource "aws_route53_record" "TestHostedZoneRecord" {
  zone_id = "${aws_route53_zone.TestHostedZone.zone_id}"
  name    = "testinstance1.testhostedzone.internal"
  type    = "A"
  ttl     = "300"
  records = ["${aws_network_interface.TestPrivateNetworkInterface.private_ips[0]}"]
}
