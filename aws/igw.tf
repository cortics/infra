resource "aws_internet_gateway" "TestIgw" {
  vpc_id = "${aws_vpc.TestVPC.id}"

  tags {
    Name = "TestIgw"
  }
}
