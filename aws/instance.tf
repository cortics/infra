resource "aws_instance" "TestPublicInstance" {
  ami               = "${data.aws_ami.TestUbuntuAMI.id}"
  instance_type     = "t2.micro"
  availability_zone = "${aws_subnet.TestPublicSubnet.availability_zone}"
  placement_group   = "${aws_placement_group.TestPublicPlacementGroup.id}"
  key_name          = "${aws_key_pair.TestKeyPair.key_name}"
  monitoring        = "true"

  #  vpc_security_group_ids = ["${aws_security_group.TestSecurityGroup.id}"]
  root_block_device {
    volume_type = "gp2"
    volume_size = 10
  }

  network_interface {
    network_interface_id = "${aws_network_interface.TestPublicNetworkInterface.id}"
    device_index         = 0
  }

  tags {
    Name = "TestPublicInstance"
  }
}

resource "aws_instance" "TestPrivateInstance" {
  ami               = "${data.aws_ami.TestUbuntuAMI.id}"
  instance_type     = "t2.micro"
  availability_zone = "${aws_subnet.TestPrivateSubnet.availability_zone}"
  placement_group   = "${aws_placement_group.TestPrivatePlacementGroup.id}"
  key_name          = "${aws_key_pair.TestKeyPair.key_name}"
  monitoring        = "true"

  # vpc_security_group_ids = ["${aws_security_group.TestSecurityGroup.id}"]
  root_block_device {
    volume_type = "gp2"
    volume_size = 10
  }

  network_interface {
    network_interface_id = "${aws_network_interface.TestPrivateNetworkInterface.id}"
    device_index         = 0
  }

  tags {
    Name = "TestPrivateInstance"
  }
}
