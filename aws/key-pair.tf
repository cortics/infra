resource "aws_key_pair" "TestKeyPair" {
  key_name   = "TestKeyPair"
  public_key = "${tls_private_key.TestTLSPrivateKey.public_key_openssh}"
}
