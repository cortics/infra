resource "aws_kms_key" "TestKMSKey" {
  description             = "TestKMSKey"
  deletion_window_in_days = 7
  is_enabled              = "true"
  enable_key_rotation     = "true"
}

resource "aws_kms_alias" "TestKMSKeyAlias" {
  name          = "alias/my-key-alias"
  target_key_id = "${aws_kms_key.TestKMSKey.key_id}"
}
