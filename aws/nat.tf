resource "aws_nat_gateway" "TestNatGw" {
  allocation_id = "${aws_eip.TestNatEip.id}"
  subnet_id     = "${aws_subnet.TestPublicSubnet.id}"

  tags {
    Name = "TestNatGw"
  }
}
