resource "aws_network_interface" "TestPrivateNetworkInterface" {
  subnet_id       = "${aws_subnet.TestPrivateSubnet.id}"
  private_ips     = ["10.62.17.10"]
  security_groups = ["${aws_security_group.TestSecurityGroup.id}"]
}

resource "aws_network_interface" "TestPublicNetworkInterface" {
  subnet_id       = "${aws_subnet.TestPublicSubnet.id}"
  private_ips     = ["10.62.16.10"]
  security_groups = ["${aws_security_group.TestSecurityGroup.id}"]
}
