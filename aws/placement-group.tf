resource "aws_placement_group" "TestPublicPlacementGroup" {
  name     = "TestPublicPlacementGroup"
  strategy = "spread"
}

resource "aws_placement_group" "TestPrivatePlacementGroup" {
  name     = "TestPrivatePlacementGroup"
  strategy = "spread"
}
