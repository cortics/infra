resource "aws_redshift_cluster" "TestRedshiftCluster" {
  cluster_identifier        = "kafka-connect"
  database_name             = "redshift"
  master_username           = "redshift"                                                  ## Dummy username.
  master_password           = "Redshift123$"                                              ## Dummy password. Change and manage appropriately.
  node_type                 = "dc2.large"
  cluster_type              = "single-node"
  vpc_security_group_ids    = ["${aws_security_group.TestSecurityGroup.id}"]
  encrypted                 = "true"
  kms_key_id                = "${aws_kms_key.TestKMSKey.arn}"
  cluster_subnet_group_name = "${aws_redshift_subnet_group.TestRedshiftSubnetGroup.name}"
  skip_final_snapshot       = "true"
}

resource "aws_redshift_subnet_group" "TestRedshiftSubnetGroup" {
  name       = "test-redshift-subnet-group"
  subnet_ids = ["${aws_subnet.TestPrivateSubnet.id}", "${aws_subnet.TestPublicSubnet.id}"]

  tags {
    environment = "Dev"
  }
}
