resource "aws_route_table" "TestPublicSubnetRouteTable" {
  vpc_id = "${aws_vpc.TestVPC.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.TestIgw.id}"
  }

  tags {
    Name = "TestPublicSubnetRouteTable"
  }
}

resource "aws_route_table_association" "TestPublicSubnetRouteTableAssociation" {
  subnet_id      = "${aws_subnet.TestPublicSubnet.id}"
  route_table_id = "${aws_route_table.TestPublicSubnetRouteTable.id}"
}

resource "aws_route_table" "TestPrivateSubnetRouteTable" {
  vpc_id = "${aws_vpc.TestVPC.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.TestNatGw.id}"
  }

  tags {
    Name = "TestPrivateSubnetRouteTable"
  }
}

resource "aws_route_table_association" "TestPrivateSubnetRouteTableAssociation" {
  subnet_id      = "${aws_subnet.TestPrivateSubnet.id}"
  route_table_id = "${aws_route_table.TestPrivateSubnetRouteTable.id}"
}
