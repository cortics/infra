resource "aws_security_group" "TestSecurityGroup" {
  name        = "TestSecurityGroup"
  description = "Testing VPC from scratch"
  vpc_id      = "${aws_vpc.TestVPC.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "TestSecurityGroup"
  }
}
