resource "aws_sqs_queue" "TestSQSQueue" {
  name                              = "TestSQSQueue.fifo"
  delay_seconds                     = 0                                  ## Max 900 (15 minutes)
  visibility_timeout_seconds        = 30                                 ## Max 43200 (12 hours)
  max_message_size                  = 1024                               ## 262144 bytes (256 KiB)
  message_retention_seconds         = 86400                              ## Max 1209600 (14 days)
  receive_wait_time_seconds         = 0                                  ## Max 20 seconds
  fifo_queue                        = true
  content_based_deduplication       = true
  kms_master_key_id                 = "${aws_kms_key.TestKMSKey.key_id}"
  kms_data_key_reuse_period_seconds = 300                                ## Max 86400 seconds (1 day)

  tags {
    Environment = "Dev"
  }
}
