resource "aws_subnet" "TestPublicSubnet" {
  vpc_id            = "${aws_vpc.TestVPC.id}"
  cidr_block        = "10.62.16.0/24"
  availability_zone = "${data.aws_availability_zones.TestAZs.names[0]}"
  depends_on        = ["aws_internet_gateway.TestIgw"]

  tags {
    Name = "TestPublicSubnet"
  }
}

resource "aws_subnet" "TestPrivateSubnet" {
  vpc_id            = "${aws_vpc.TestVPC.id}"
  cidr_block        = "10.62.17.0/24"
  availability_zone = "${data.aws_availability_zones.TestAZs.names[1]}"

  tags {
    Name = "TestPrivateSubnet"
  }
}
