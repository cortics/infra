resource "tls_private_key" "TestTLSPrivateKey" {
  algorithm = "RSA"
  rsa_bits  = 2048
}
