resource "aws_ebs_volume" "TestPublicVolume" {
  availability_zone = "${aws_subnet.TestPublicSubnet.availability_zone}"
  encrypted         = "true"
  type              = "gp2"
  size              = 40

  tags {
    Name = "TestPublicVolume"
  }
}

resource "aws_ebs_volume" "TestPrivateVolume" {
  availability_zone = "${aws_subnet.TestPrivateSubnet.availability_zone}"
  encrypted         = "true"
  type              = "gp2"
  size              = 40

  tags {
    Name = "TestPrivateVolume"
  }
}

resource "aws_volume_attachment" "TestPrivateVolumeAttachment" {
  device_name = "/dev/xvdb"
  volume_id   = "${aws_ebs_volume.TestPrivateVolume.id}"
  instance_id = "${aws_instance.TestPrivateInstance.id}"
}

resource "aws_volume_attachment" "TestPublicVolumeAttachment" {
  device_name = "/dev/xvdb"
  volume_id   = "${aws_ebs_volume.TestPublicVolume.id}"
  instance_id = "${aws_instance.TestPublicInstance.id}"
}

resource "aws_ebs_snapshot" "TestPublicVolumeSnapshot" {
  volume_id = "${aws_ebs_volume.TestPublicVolume.id}"

  tags {
    Name = "TestPublicVolumeSnapshot"
  }
}

resource "aws_ebs_snapshot" "TestPrivateVolumeSnapshot" {
  volume_id = "${aws_ebs_volume.TestPrivateVolume.id}"

  tags {
    Name = "TestPrivateVolumeSnapshot"
  }
}
