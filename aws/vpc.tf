resource "aws_vpc" "TestVPC" {
  cidr_block           = "10.62.16.0/23"
  instance_tenancy     = "default"
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags {
    Name = "TestVPC"
  }
}
