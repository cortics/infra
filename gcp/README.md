## Google Cloud Platform

This section provides example Terraform resources for the most commonly used components in the [Google Cloud Platform](https://cloud.google.com).

#### Setting up credentials.

You will need to create a project in the Google Cloud Console and enable billing. You will also need to create and download a Google Cloud credentials JSON file. The credentials file is usually placed in a hidden directory in your `$HOME`. More on generating credentials [here](https://cloud.google.com/iam/docs/creating-managing-service-account-keys).

You will need to update `setup-credentials.sh` in this folder to add your project name, region and full path to your credentials file. Run the file to configure your access to GCP.

```bash
source setup-credentials.sh
```
