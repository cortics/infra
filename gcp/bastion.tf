resource "google_compute_address" "TestBastionIp" {
  name = "test-bastion-ip"
}

resource "google_compute_instance" "TestBastionInstance" {
  name         = "test-bastion-instance"
  machine_type = "g1-small"
  zone         = "us-central1-f"
  can_ip_forward = "true"
  boot_disk {
    initialize_params {
      size = 10
      type = "pd-ssd"
      image = "ubuntu-1604-lts"
    }
  }

  metadata {
    ssh-keys = "ubuntu:${tls_private_key.TestBastionKey.public_key_pem}"
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.TestGoogleComputePublicSubnetwork.name}"
    access_config {
      nat_ip = "${google_compute_address.TestBastionIp.address}"
    }
  }

}
