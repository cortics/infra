resource "google_compute_firewall" "TestBastionFirewall" {
  name    = "test-bastion-firewall"
  network = "${google_compute_network.TestGoogleComputeNetwork.name}"
  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "TestInternalFirewall" {
  name    = "test-internal-firewall"
  network = "${google_compute_network.TestGoogleComputeNetwork.name}"
  source_ranges = ["10.181.0.0/16"]

  allow {
    protocol = "tcp"
    ports = ["1-65535"]
  }
  allow {
    protocol = "udp"
    ports = ["1-65535"]
  }
  allow {
    protocol = "icmp"
  }
}
