resource "google_compute_address" "TestNatPublicIp" {
  name = "test-nat-public-ip"
}

resource "google_compute_instance" "TestNatInstance" {
  name         = "test-nat-instance"
  machine_type = "g1-small"
  zone         = "us-central1-f"
  can_ip_forward = "true"
  tags = ["nat"]
  boot_disk {
    initialize_params {
      size = 10
      type = "pd-ssd"
      image = "ubuntu-1604-lts"
    }
  }

  metadata {
    ssh-keys = "ubuntu:${tls_private_key.TestNatKey.public_key_pem}"
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.TestGoogleComputePublicSubnetwork.name}"
    access_config {
      nat_ip = "${google_compute_address.TestNatPublicIp.address}"
    }
  }

}
