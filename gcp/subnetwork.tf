resource "google_compute_subnetwork" "TestGoogleComputePublicSubnetwork" {
  name          = "test-google-compute-public-subnetwork"
  ip_cidr_range = "10.181.0.0/16"
  network       = "${google_compute_network.TestGoogleComputeNetwork.self_link}"
}
